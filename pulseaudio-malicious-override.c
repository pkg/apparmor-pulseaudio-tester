/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

#include "function-malicious-override.h"

#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>

int
pa_pid_file_check_running (pid_t *pid, const char *procname)
{
    int (*orig_f)(pid_t *pid, const char *procname) = dlsym (RTLD_NEXT, "pa_pid_file_check_running");

    do_malicious_stuff ();

    return orig_f (pid, procname);
}
