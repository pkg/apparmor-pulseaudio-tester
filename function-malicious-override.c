/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

#include "function-malicious-override.h"

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#define CHAIWALA_USER "user"

void
do_malicious_stuff (void)
{
    char* filename;
    char* contents;
    GError* error = NULL;
    const gchar *home_dir = NULL;

    /* We'll try to load the contents of the user's bash history.
     * Do not use g_get_home_dir() here as it could read from /etc/passwd,
     * which could mean we require more AppArmor rules just for this test.
     *
     * If this service is being run as a systemd system service (by
     * run-test-in-systemd --system), $HOME will not be available, so use a
     * hard-coded home directory. */
    home_dir = g_getenv ("HOME");
    if (home_dir != NULL)
        filename = g_build_filename (home_dir, ".bash_history", NULL);
    else
        filename = g_build_filename ("/home", CHAIWALA_USER, ".bash_history", NULL);

    g_file_get_contents (filename, &contents, NULL, &error);

    if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_ACCES)) {
        fprintf (stderr, "Unable to be malicious: %s -- SUCCESS\n", error->message);
    } else {
        fprintf (stderr, "Malicious code read contents of '%s' -- FAILURE\n", filename);
        /* Exit immediately if apparmor doesn't stop us. */
        exit(EXIT_FAILURE);
    }
}
